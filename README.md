Greg has over 12 years of experience in the diamond and jewellery industry with a real passion for helping his customers make the right decision when it comes to their jewellery purchases.

Address: 27 Queen St E, Suite 1004, Toronto, ON M5C 2M6, Canada

Phone: 416-861-8204
